<?php
//Classe genérica para conexão com o Banco, banco abstraído em PDO.


class crud_geral
{

  protected static $tabela ;
  protected static $campo_id;

  function __construct()
  {
    global $path;
    global $db_string;

    $this->obj_db = new PDO($db_string); // TEMP

    // $this->obj_db = new PDO("sqlite:db/banco.sqlite"); //DEBUG
    $this->obj_db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $this->conn = new StdClass();
    $this->conn->tabela = static::$tabela;
    $this->conn->campo_id = static::$campo_id;

    $this->dados = new StdClass();
    $this->lista_todos();
    $this->zera_properties();
    $this->num_registros();
  }

  function zera_properties()
  {
    foreach ($this->dados as $key => $value) {
        // echo $this->dados->$key = NULL; //DEBUG
    }
  }

  function lista_todos()
  {
    $tabela = $this->conn->tabela;
    $return = $this->obj_db->query("select * from $tabela");
    $result = $return->fetchAll(PDO::FETCH_ASSOC);
    // print_r($result); //passar isso aqui para o objeto $this->dados  //DEBUG

    $this->todos = new StdClass();
    $this->todos = $result;

    return $result; //SAIDA DUAL
  }

  function num_registros()
  {
    $tabela = $this->conn->tabela;
    $return = $this->obj_db->query("select count(*) from $tabela");
    $result = $return->fetchAll(PDO::FETCH_ASSOC);
    // print_r($result); //passar isso aqui para o objeto $this->dados  //DEBUG

    $this->num_registros = $result;

    return $result; //SAIDA DUAL
  }

  function seleciona($id)
  {
    if(!isset($id))
    {
      return false;
      exit;
    }
    $tabela = $this->conn->tabela;
    $campo_id = $this->conn->campo_id;

    $this->zera_properties();

    try {
      $return = $this->obj_db->query("select * from $tabela where $campo_id = '$id'");

    }
    catch(Exception $e) {
        echo 'Exception -> ';
        var_dump($e->getMessage());
    }

    //echo  "select * from $tabela where $campo_id = '$id'"; // DEBUG
    $result = $return->fetch(PDO::FETCH_ASSOC);

    // var_dump($return); //DEBUG

    foreach ($result as $key => $value) {
       $this->dados->$key = $value;       //Escreve as vars no objeto
    }
    //var_dump($this->dados); //DEBUG
    return $result; //Colocar na documentaao que é funcao DUAL (array e objeto)
  }

  function seleciona_placa($placa)
  {
    if(!isset($placa))
    {
      return false;
      exit;
    }
    $tabela = $this->conn->tabela;

    $this->zera_properties();

    try {
      $return = $this->obj_db->query("select * from $tabela where placa = '$placa' order by id desc limit 1");

    }
    catch(Exception $e) {
        echo 'Exception -> ';
        var_dump($e->getMessage());
    }

    //echo  "select * from $tabela where $campo_id = '$id'"; // DEBUG
    $result = $return->fetch(PDO::FETCH_ASSOC);

    // var_dump($return); //DEBUG

    foreach ($result as $key => $value) {
       $this->dados->$key = $value;       //Escreve as vars no objeto
    }
    //var_dump($this->dados); //DEBUG
    return $result; //Colocar na documentaao que é funcao DUAL (array e objeto)
  }


  function deleta($id)
  {
    if(!isset($id))
    {
      return false;
      exit;
    }
    $tabela = $this->conn->tabela;
    $campo_id = $this->conn->campo_id;
    try {
      $return = $this->obj_db->exec("delete from $tabela where $campo_id = '$id'");

    }
    catch(Exception $e) {
        echo 'Exception -> ';
        var_dump($e->getMessage());
        exit;
    }
    return $return;
  }

  function insere()
  {
    $tabela = $this->conn->tabela;

    $sql_query =
    "
    insert into $tabela
    (";

    foreach ($this->dados as $key => $value) {
    $sql_query .= $key . ',
    ';
      }

    $sql_query .= "log)
    values
    (
    ";
    foreach ($this->dados as $key => $value) {
    $sql_query .= $this->obj_db->quote($value). ","
    ;
      }

    $sql_query .= time().")
    ";

    try {
      $this->obj_db->exec($sql_query);

    }
    catch(Exception $e) {
        echo 'Exception -> ';
        echo "$sql_query<br>";
        var_dump($e->getMessage());
        exit;
    }
    //  echo $sql_query; //DEBUG

    return $this->obj_db->lastInsertId();
  }

  function atualiza($id) //Avisar que o front-end tem que mandar os dados todos, nao só a alteracao
  {
    $tabela = $this->conn->tabela;
    $campo_id = $this->conn->campo_id;

    $sql_query =
    "
    update $tabela set
    ";

    foreach ($this->dados as $key => $value) {
    $sql_query .= $key . ' = ' .$value . ', ';
      }

    $sql_query .= "
    where $campo_id = '$id'
    ";
    try {
      $this->obj_db->exec($sql_query);

    }
    catch(Exception $e) {
        echo 'Exception -> ';
        echo "$sql_query<br>";
        var_dump($e->getMessage());
        exit;
    }

    return true;
  }
  function saida_placa($placa) //Avisar que o front-end tem que mandar os dados todos, nao só a alteracao
  {
    $tabela = $this->conn->tabela;

    $this->dados->hsaida = time();


    $sql_query =
    "
    update $tabela set
    ";

    foreach ($this->dados as $key => $value) {
    if($value != '') $sql_query .= $key . ' = ' .$value . ', ';
      }

    $sql_query = substr($sql_query, 0, -2);

    $sql_query .= "
    where placa = '$placa' and hsaida = ''
    ";

    // var_dump($sql_query); //DEBUG
    // exit;                  //DEBUG

    try {
      $this->obj_db->exec($sql_query);

      global $db_string;
      // var_dump($db_string);


    }
    catch(Exception $e) {
        echo 'Exception -> ';
        echo "$sql_query<br>";
        var_dump($e->getMessage());
        exit;
    }

    return true;
  }

}
  ?>
