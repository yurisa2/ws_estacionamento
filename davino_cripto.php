<?php

$chave_cripto = "FATEC20181234567";
$texto_entrada = "Texto de entrada da cripto";
$texto_criptografado = "";
$texto_saida_descriptografado = "";



function Criptografa($texto,$chave_cripto)
{
  $crypt = new Encryption($chave_cripto);
  $retorno = $crypt->encrypt($texto);
  return $retorno;
}

function DesCriptografa($texto,$chave_cripto)
{
  $crypt = new Encryption($chave_cripto);
  $retorno = $crypt->decrypt($texto);
  return $retorno;
}

class Encryption //100% StackOverflow
{
  const CIPHER = MCRYPT_RIJNDAEL_128; // Rijndael-128 is AES
  const MODE   = MCRYPT_MODE_CBC;

  /* Cryptographic key of length 16, 24 or 32. NOT a password! */
  private $key;
  public function __construct($key) {
    $this->key = $key;
  }

  public function encrypt($plaintext) {
    $ivSize = mcrypt_get_iv_size(self::CIPHER, self::MODE);
    $iv = mcrypt_create_iv($ivSize, MCRYPT_DEV_URANDOM);
    $ciphertext = mcrypt_encrypt(self::CIPHER, $this->key, $plaintext, self::MODE, $iv);
    return base64_encode($iv.$ciphertext);
  }

  public function decrypt($ciphertext) {
    $ciphertext = base64_decode($ciphertext);
    $ivSize = mcrypt_get_iv_size(self::CIPHER, self::MODE);
    if (strlen($ciphertext) < $ivSize) {
      throw new Exception('Missing initialization vector');
    }

    $iv = substr($ciphertext, 0, $ivSize);
    $ciphertext = substr($ciphertext, $ivSize);
    $plaintext = mcrypt_decrypt(self::CIPHER, $this->key, $ciphertext, self::MODE, $iv);
    return rtrim($plaintext, "\0");
  }
}


echo "<pre> texto de entrada: ".$texto_entrada;
echo "<br>";
$texto_criptografado = Criptografa($texto_entrada,$chave_cripto);
echo "texto criptografado: ".$texto_criptografado;
$texto_saida_descriptografado = DesCriptografa($texto_criptografado,$chave_cripto);
echo "<br> texto descriptografado: ".$texto_saida_descriptografado;



 ?>
