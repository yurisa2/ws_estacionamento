<?php
$path = '../';
require_once '../include/include_all.php';

$xml_recebido = $_POST["xml_envio"];

class ws_estacionamento
{
  public function __construct()
  {
    global $db_string;
    global $xml_recebido;
    $this->saida = NULL;
    $xmlstr = $xml_recebido;
    $this->xml =  new SimpleXMLElement($xmlstr);

    $this->auten = new autenticacao($this->xml->usuario,$this->xml->senha);
    // $this->auten = new autenticacao("yuri","sa");
    if($this->auten->autenticacao == true)
    {
      $this->estacionamento = new estacionamento;  //é'um crud especifico'
      $this->io($this->xml->io);
    }
    if($this->auten->autenticacao == false)
    {
      $this->saida_login_invalido();
    }
  }

  function saida_login_invalido()
  {
    $this->escritor_xml = new Xml;
    $this->escritor_xml->OpenTag("dados");
    $this->escritor_xml->addTag("ERRO","Login Invalido");
    $this->escritor_xml->CloseTag("dados");

    $this->saida = $this->escritor_xml->__toString();

  }

  function io($tipoIO) //Verifica se é entrada ou saida para escrever os dados certos
  {
    if($tipoIO == "Entrada")
    {
      $this->estacionamento->dados->placa = (string)$this->xml->placa;
      $this->estacionamento->dados->modelo = (string)$this->xml->modelo;
      $this->estacionamento->dados->cor = (string)$this->xml->cor;
      $this->estacionamento->dados->hentrada = time();
      $inserir = $this->estacionamento->insere();
    }
    if($tipoIO == "Saida")
    {
      $this->estacionamento->saida_placa($this->xml->placa);
    }
    if($tipoIO == "Consulta")
    {
      $this->estacionamento->dados->placa = (string)$this->xml->placa;
      $this->estacionamento->seleciona_placa($this->estacionamento->dados->placa);


      $this->escritor_xml = new Xml;
      $this->escritor_xml->OpenTag("dados");
      $this->escritor_xml->addTag("io","Consulta");
      $this->escritor_xml->addTag("placa",$this->estacionamento->dados->placa);
      $this->escritor_xml->addTag("modelo",$this->estacionamento->dados->modelo);
      $this->escritor_xml->addTag("hentrada",$this->estacionamento->dados->hentrada);
      $this->escritor_xml->addTag("hsaida",$this->estacionamento->dados->hsaida);
      $this->escritor_xml->addTag("cor",$this->estacionamento->dados->cor);
      $this->escritor_xml->CloseTag("dados");

      $this->saida = $this->escritor_xml->__toString();
      // var_dump($this->escritor_xml->__toString());
    }


  }
}


class Encryption //100% StackOverflow
{
  const CIPHER = MCRYPT_RIJNDAEL_128; // Rijndael-128 is AES
  const MODE   = MCRYPT_MODE_CBC;

  /* Cryptographic key of length 16, 24 or 32. NOT a password! */
  private $key;
  public function __construct($key) {
    $this->key = $key;
  }

  public function encrypt($plaintext) {
    $ivSize = mcrypt_get_iv_size(self::CIPHER, self::MODE);
    $iv = mcrypt_create_iv($ivSize, MCRYPT_DEV_URANDOM);
    $ciphertext = mcrypt_encrypt(self::CIPHER, $this->key, $plaintext, self::MODE, $iv);
    return base64_encode($iv.$ciphertext);
  }

  public function decrypt($ciphertext) {
    $ciphertext = base64_decode($ciphertext);
    $ivSize = mcrypt_get_iv_size(self::CIPHER, self::MODE);
    if (strlen($ciphertext) < $ivSize) {
      throw new Exception('Missing initialization vector');
    }

    $iv = substr($ciphertext, 0, $ivSize);
    $ciphertext = substr($ciphertext, $ivSize);
    $plaintext = mcrypt_decrypt(self::CIPHER, $this->key, $ciphertext, self::MODE, $iv);
    return rtrim($plaintext, "\0");
  }
}

class autenticacao {
  public function __construct($user,$pass) {
    $this->obj_aut =  new usuarios;
    $this->user = $user;
    $this->pass = $pass;
    $this->retorna_chave();
    $this->autenticacao = $this->verifica_pass($this->chave);


  }

  function retorna_chave() {
    foreach ($this->obj_aut->todos as $key => $value) {
      if($this->user == $value["username"])
      {
        $this->chave = $key;
      }
    }
  }

  function verifica_pass($chave) {
    // echo 'user: '.$this->obj_aut->todos[$chave]["username"]; //DEBUG
    // echo '<br>pass: '.$this->obj_aut->todos[$chave]["password"]; //DEBUG
    // if($this->user == $this->obj_aut->todos[$chave]["username"]) echo 'User OK'; //DEBUG
    // if($this->pass == $this->obj_aut->todos[$chave]["password"]) echo 'Pass OK'; //DEBUG

    if($this->user == $this->obj_aut->todos[$chave]["username"] &&
        $this->pass == $this->obj_aut->todos[$chave]["password"])
        return true;
        else{
          // echo 'Nome de usuário ou senha incorretos';
          return false;
        }
    // var_dump($this->obj_aut->todos[$chave]);
    // var_dump($this->obj_aut->todos);
  }

  function cripto_token()
  {
    if($this->verifica_pass($this->chave))
    {
    $crypt = new Encryption("FATEC20181234567");
    $string = $this->user.'-'.$this->pass.'-'.(time()+3600);
    var_dump($string);
    $retorno = $crypt->encrypt($string);
    return $retorno;
    }
    else return false;
  }

  function descripto_token($token)
  {
    $crypt = new Encryption("FATEC20181234567");
    $string = $token;
    $retorno = $crypt->decrypt($string);
    return $retorno;
  }

  function verifica_token($token)
  {
    $hora_token =  substr($this->descripto_token($token), -10);
    if($hora_token > time()) return true;
    else return false;
  }

}

// echo '<pre>v1.002<br>';
//
// $objeto = new autenticacao("yuri","sa");
// $token_cripto = $objeto->cripto_token();
// var_dump($token_cripto);
// $token_descripto =  $objeto->descripto_token($token_cripto);
// var_dump($token_descripto);
// $token_hora =  $objeto->verifica_token($token_cripto);
// var_dump($token_hora);
//
//
// exit;

$ws_server = new ws_estacionamento;
// var_dump($ws_server->saida);
$Cripto_OO = new Encryption("FATEC20181234567");
$saida_cripto = $Cripto_OO->encrypt($ws_server->saida);


//echo $ws_server->saida; //resposta da saida do WS
echo $saida_cripto; //resposta da saida do WS Criptografada

?>
